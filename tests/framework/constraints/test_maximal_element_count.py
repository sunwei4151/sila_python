from sila2.framework.constraints.maximal_element_count import MaximalElementCount


def test():
    c = MaximalElementCount(3)

    assert repr(c) == "MaximalElementCount(3)"

    assert c.validate([1, 2])
    assert c.validate([1, 2, 3])
    assert not c.validate([1, 2, 3, 4])
