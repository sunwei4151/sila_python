import pytest
from pytest import fixture

from sila2.framework.data_types.binary import Binary
from sila2.framework.pb2 import SiLAFramework_pb2


@fixture
def binary_field() -> Binary:
    return Binary()


def test_to_native_type():
    msg = SiLAFramework_pb2.Binary(value=b"abc")
    val = Binary().to_native_type(msg)  # use GreetingProvider

    assert isinstance(val, bytes)
    assert val == b"abc"


def test_to_message(binary_field):
    msg = binary_field.to_message(b"abc")
    assert msg.value == b"abc"


def test_wrong_type(binary_field):
    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        binary_field.to_message(1)
    with pytest.raises(TypeError):
        # noinspection PyTypeChecker
        binary_field.to_message("")
    with pytest.raises(ValueError):
        binary_field.to_native_type(SiLAFramework_pb2.Binary())
