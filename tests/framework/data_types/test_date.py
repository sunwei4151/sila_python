from datetime import date, timedelta, timezone, tzinfo

import pytest

from sila2.framework.data_types.date import SilaDateType


def test_to_native_type(basic_feature):
    date_field = basic_feature._data_type_definitions["Date"].data_type
    SilaDate = date_field.message_type
    SilaTimezone = basic_feature._pb2_module.SiLAFramework__pb2.Timezone

    msg = SilaDate(day=1, month=2, year=2000, timezone=SilaTimezone(hours=3, minutes=30))
    native = date_field.to_native_type(msg)
    assert isinstance(native, SilaDateType)
    d, tz = native

    assert isinstance(d, date)
    assert d.day == 1
    assert d.month == 2
    assert d.year == 2000

    assert isinstance(tz, tzinfo)
    assert tz.utcoffset(None) == timedelta(hours=3, minutes=30)


def test_to_message(basic_feature):
    date_field = basic_feature._data_type_definitions["Date"].data_type
    SilaDate = date_field.message_type

    msg = date_field.to_message(SilaDateType(date(day=1, month=2, year=2000), timezone(timedelta(hours=3, minutes=30))))

    assert isinstance(msg, SilaDate)
    assert msg.day == 1
    assert msg.month == 2
    assert msg.year == 2000
    assert msg.timezone.hours == 3
    assert msg.timezone.minutes == 30


def test_seconds_in_timezone(basic_feature):
    date_field = basic_feature._data_type_definitions["Date"].data_type

    with pytest.raises(ValueError):
        _ = date_field.to_message(
            SilaDateType(date(day=1, month=2, year=2000), timezone(timedelta(hours=3, minutes=30, seconds=10)))
        )


def test_from_string(basic_feature):
    date_field = basic_feature._data_type_definitions["Date"].data_type

    assert date_field.from_string("2021-08-18Z") == (date(2021, 8, 18), timezone(timedelta(0)))
