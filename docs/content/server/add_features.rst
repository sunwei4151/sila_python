Add features to existing SiLA Server/Client package
===================================================

To add new SiLA features to an existing package, run ``sila2-codegen add-features Feature1.sila.xml Feature2.sila.xml ...`` from the package directory (or specify the package directory with ``--package-dir``). See also :doc:`/content/code_generator/add_features`.

This will add and update the required files in the ``generated`` directory, add ``..._impl.py`` files in the ``feature_implementations`` directory, and add the feature to the SiLA Server class in ``server.py``.
