[project]
name = "sila2"
version = "0.10.4"
description = "Python implementation of the SiLA 2 standard for lab automation"
readme = "README.md"
authors = [
    { name = "Niklas Mertsch", email = "niklas.mertsch@wega-it.com" },
]
license = {file = "LICENSE"}
classifiers = [
    "Development Status :: 4 - Beta",
    "Intended Audience :: Developers",
    "Intended Audience :: Science/Research",
    "Intended Audience :: Healthcare Industry",
    "License :: OSI Approved :: MIT License",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 3",
]
requires-python = ">= 3.7"
dependencies = [
    "grpcio >= 1.44.0",
    "grpcio-tools >= 1.44.0",
    "importlib-metadata; python_version < '3.8'",
    "lxml >= 4.4.0",
    "typing-extensions",
    "zeroconf",
]

[project.optional-dependencies]
test = [
    "pytest",
    "pytest-cov",
]
dev = [
    "black",
    "isort",
    "pytest",
    "pytest-cov",
    "pyproject-flake8",
]
docs = [
    "sphinx",
    "sphinxcontrib-napoleon",
    "sphinxcontrib-runcmd",
]
codegen = [
    "black",
    "isort",
    "jinja2",
    "typer",
]
cryptography = ["cryptography >= 36.0.0"]
jsonschema = ["jsonschema"]
xmlschema = ["xmlschema"]
full = [
    "black",
    "cryptography",
    "isort",
    "jinja2",
    "jsonschema",
    # flake8 does not support pyproject.toml config natively; pyproject-flake8 does not support flake8 >= 5.0.0
    "flake8 < 5.0.0",
    "pyproject-flake8",
    "pytest",
    "pytest-cov",
    "sphinx",
    "sphinxcontrib-napoleon",
    "sphinxcontrib-runcmd",
    "typer",
    "xmlschema",
]

[project.urls]
"Homepage" = "https://gitlab.com/sila2/sila_python"
"Repository" = "https://gitlab.com/sila2/sila_python"
"Documentation" = "https://sila2.gitlab.io/sila_python"
"Bug Tracker" = "https://gitlab.com/sila2/sila_python/-/issues"
"SiLA Standard" = "https://sila-standard.org"

[project.scripts]
sila2-codegen = "sila2.code_generator.__main__:main"

[tool.setuptools]
package-dir = {"" = "src"}

[tool.setuptools.package-data]
"sila2.features" = ["*/*.sila.xml"]
"sila2.resources.xsl" = ["*.xsl"]
"sila2.resources.xsd" = ["*.xsd"]
"sila2.resources.proto" = ["*.proto"]
"sila2.resources.code_generator_templates" = ["*/*.jinja2"]

[build-system]
requires = ["setuptools>=0.61.0"]
build-backend = "setuptools.build_meta"

[tool.black]
line-length = 120
fast = true
force-exclude = "sila_base/"

[tool.coverage.run]
branch = true

[tool.coverage.report]
exclude_lines = [
    "@abstractmethod",
    "@abc.abstractmethod",
    "def __repr__(self):",
    # re-enable the standard pragma
    "pragma: no cover",
    "raise NotImplementedError",
    "if TYPE_CHECKING",
]
omit = [
    "setup.py",
    # auto-generated by grpcio_tools.protoc
    "*_pb2.py",
    # will be copied to the server, so this source code is never executed
    "src/sila2/server/default_feature_implementations/*",
    "src/sila2/features/*",
]
precision = 2

[tool.isort]
line_length = 120
profile = "black"
skip = [
    "sila_base/",
    "venv",
]

[tool.flake8]
max-line-length = 120
extend-ignore = "E203,E501,W293"
exclude = [
    "sila_base/",
    "venv",
]

[tool.pytest.ini_options]
filterwarnings = [
    # many servers are started and stopped rapidly during tests, zeroconf can't catch up (which is fine), causing this warning
    "ignore:Failed to connect .* SilaConnectionError:RuntimeWarning",
    # https://github.com/pypa/pip/issues/11975
    "ignore:.*pkg_resources.*:DeprecationWarning",
]
